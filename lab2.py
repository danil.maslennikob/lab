import math

my_fun = input("Функция которую хотите посчитать: ")

if my_fun in ['G', 'F', 'Y']:
    pass
else:
    exit('Неверное название функции')

try:
    #Введение переменных 
    a = float(input("Введите a: ")) 
    x = float(input("Введите x: "))
except ValueError:
    exit('ValueError')   #Неверное введённое значение

if my_fun == 'G':
    try:
        G = (-3 * (14 * a**2 + 23 * a * x - 30 * x**2)) / (-9 * a**2 + 37 * a * x + 40 * x**2)
    except ZeroDivisionError:
        exit('Zero Division Error')
    print('Fiction G = {:.5f}'.format(G))

if my_fun == "F":
    try:
        F = -math.tan(18 * a**2 - a * x - 4 * x**2)
    except ValueError:
        exit('Value Error')
    print('Fiction F = {:.5f}'.format(F))

elif my_fun == 'Y':
    try:
        Y = math.log(35  * a**2 - 27 * a * x + 4 * x**2 + 1) / math.log(2)
    except ValueError:
        exit('Value Error')
    print('Fiction Y = {:.5f}'.format(Y))
