import matplotlib.pyplot as plt
import math

while True:
    my_fun = input('Функция, которую вы хотите посчитать: ')
    if my_fun in ['g', 'f', 'y']:
        pass
    else:
        exit('Неправильное название функции')

    x_list = []
    y_list = []

    try:
        x = float(input('Введите значение переменной x для функции: '))
        a = float(input('Введите значение переменной a для функции: '))
        x_max = float(input('Введите максимальное значение x: '))
        x_step = float(input('Введите количество шагов: '))
    except ValueError:
        exit('Вы ввели неверное значение ("ValueError")')

    if my_fun == 'g':
        while x < x_max:
            try:
                g = (-3 * (4 * a**2 + 23 * a * - 30 * x**2))/(-9 * a**2 + 37 * a * x + 40 * x**2)
                x_list.append(x), y_list.append(g)
                x += x_step
            except ZeroDivisionError:
                x_list.append(x), y_list.append(None)
                print('На 0 делить нельзя!')
                break

    if my_fun == "f":
        while x < x_max:
            if (18 * a**2 - a * x - 4 * x**2) == math.pi / 2 or (18 * a**2 - a * x - 4 * x**2) == 3 * math.pi / 2:
                x_list.append(x), y_list.append(None)
            f = -math.tan(18 * a**2 - a * x - 4 * x**2)
            x_list.append(x), y_list.append(f)
            x += x_step

    if my_fun == 'y':
        while x < x_max:
            try:
                y = math.log(35  * a**2 - 27 * a * x + 4 * x**2 + 1) / math.log(2)
                x_list.append(x), y_list.append(y)
                x += x_step
            except ValueError:
                x_list.append(x), y_list.append(None)
                print('Подлогарифмическое выражение должно быть > 0!')
                break


    plt.plot(x_list, y_list, 'k')
    plt.show()

    re = input('Вы хотите выйти yes|no')
    if re == 'yes':
        break
    elif re == 'no':
        pass
    else:
        exit('Вы ввели неверное значение')