import matplotlib.pyplot as plt
import math


while True:
    my_function = input('Функция, которую вы хотите посчитать: ')  # Выбор функции для подсчета
    if my_function in ['g', 'f', 'y']:
        pass
    else:
        print('Неправильное выбор функции!\n')
        continue

    # Создание массивов для хранения значений x и y
    x_list = []
    y_list = []

    x = a = x_max = x_step = 0 # Объявление переменных

    # Проверка значений, введенных пользователем
    try:
        x = float(input('Введите значение переменной x для функции: '))
        a = float(input('Введите значение переменной a для функции: '))
        x_max = float(input('Введите максимальное значение x: '))
        x_step = float(input('Введите интервал: '))
    except ValueError:
        print('Вы ввели неверное значение ("ValueError")')

    # Подсчет функции g
    if my_function == 'g':
        while x < x_max:
            try:
                g = (-3 * (4 * a**2 + 23 * a * - 30 * x**2))/(-9 * a**2 + 37 * a * x + 40 * x**2)
                x_list.append(x), y_list.append(g)
                x += x_step
                print(f'x = {x} \t y = {g}\n')
            except ZeroDivisionError:
                x_list.append(x), y_list.append(None)
                print(f'x = {x} \t y = {None}')

    # Подсчет функции f
    if my_function == 'f':
        while x < x_max:
            if (18 * a ** 2 - a * x - 4 * x ** 2) == math.pi / 2 or (18 * a ** 2 - a * x - 4 * x ** 2) == 3 * math.pi / 2:
                x_list.append(x), y_list.append(None)
                print(f'x = {x} \t y = {None}')
            f = -math.tan(18 * a ** 2 - a * x - 4 * x ** 2)
            x_list.append(x), y_list.append(f)
            print(f'x = {x} \t y = {f}\n')
            x += x_step

    # Подсчет функции y
    if my_function == 'y':
        while x < x_max:
            try:
                y = math.log(35 * a ** 2 - 27 * a * x + 4 * x ** 2 + 1) / math.log(2)
                x_list.append(x), y_list.append(y)
                print(f'x = {x} \t y = {y}\n')
                x += x_step
            except ValueError:
                x_list.append(x), y_list.append(None)
                print(f'x = {x} \t y = {None}')
                print('Подлогарифмическое выражение должно быть > 0!')
                break

    #построение графика
    plt.plot(x_list, y_list, 'k')
    plt.show()
    y_list = list(filter(None, y_list))

    # Вывод минимальных и максимальных значений в y_list
    print('Минимальный элемент y_list:', min(y_list))
    print('Максимальный элемент y_list:', max(y_list))

    # Вывод количества совпадений числа, которое искал пользователь
    search = input('Число, которое вы хотите найти:\n')

    y_list = ', '.join([str(e) for e in y_list])
    print(y_list)
    result_y = y_list.count(search)

    # Вывод найденных совпадений в x_list и y_list
    print(f'Найденно совпадений в списке y: {result_y}')

    # счет количества четных цифр в введеном числе, если оно целое, иначе выводится 0
    number = float(input('Введите число для определения в нем количества четных цифр: >'))
    count = 0
    if number == int(number):
        while number != 0:
            if number % 2 == 0:
                count += 1
            number = number // 10
        print(f'Количество четных цифр: {count}')
    else:
        print('число нецелое', 0)

    # Завершение и повотор программы
    re = input('Если вы хотите выйти, нажмите yes, в ином случае наберите любое другое значение')
    if re == 'yes':
        break
    else:
        print('Программа повторяется заново')
        continue