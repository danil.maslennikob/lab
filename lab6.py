from func_labs import g_fun, f_fun, y_fun


while True:
    # Создание массива для хранения значений x и y
    my_list = [[], [], [], []]

    # Ввод значений
    x = float(input('Введите значение переменной x для функции: '))
    a = float(input('Введите значение переменной a для функции: '))
    x_max = float(input('Введите максимальное значение x: '))
    x_step = float(input('Введите количество шагов: '))

    while x < x_max:
        try:
            g = g_fun(x,a)
            my_list[0].append(x), my_list[1].append(g)
            x += x_step
        except ZeroDivisionError:
            my_list[0].append(x), my_list[1].append(None)
        try:
            f = f_fun(x,a)
            my_list[0].append(x), my_list[2].append(f)
            x += x_step
        except OverflowError or ValueError:
            my_list[0].append(x), my_list[2].append(None)
        try:
            y = y_fun(x,a)
            my_list[0].append(x), my_list[3].append(y)
            x += x_step
        except ValueError:
            my_list[0].append(x),my_list[3].append(None)

    print(f'x_list: {my_list[0]}\ny_list: {my_list[1]}\n')
    print(f'x_list: {my_list[0]}\ny_list: {my_list[2]}\n')
    print(f'x_list: {my_list[0]}\ny_list: {my_list[3]}\n')

    re = input('Если вы хотите выйти, нажмите yes, в ином случае наберите любое другое значение ')
    if re == 'yes':
        break
    else:
        print('Повторный запуск программы ...')
        continue
