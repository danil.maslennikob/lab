import math

def g_fun(x,a):
    return (-3 * (4 * a**2 + 23 * a * - 30 * x**2))/(-9 * a**2 + 37 * a * x + 40 * x**2)

def f_fun(x,a):
    return (-math.tan(18 * a ** 2 - a * x - 4 * x ** 2))

def y_fun(x,a):
    return(math.log(35  * a**2 - 27 * a * x + 4 * x**2 + 1) / math.log(2))