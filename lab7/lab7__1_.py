from func_labs import g_fun, f_fun, y_fun


while True:
    # Создание массива для хранения значений x и y
    my_list = [[], [], [], []]
    # Ввод значений
    x = float(input('Введите значение переменной x для функции: '))
    a = float(input('Введите значение переменной a для функции: '))
    x_max = float(input('Введите максимальное значение x: '))
    x_step = float(input('Введите количество шагов: '))

    while x < x_max:
        try:
            g = g_fun(x, a)
            my_list[3].append(x), my_list[0].append(g)
            x += x_step
        except ZeroDivisionError:
            my_list[3].append(x), my_list[0].append(None)
        try:
            f = f_fun(x, a)
            my_list[3].append(x), my_list[1].append(f)
            x += x_step
        except OverflowError or ValueError:
            my_list[3].append(x), my_list[1].append(None)
        try:
            y = y_fun(x, a)
            my_list[3].append(x), my_list[2].append(y)
            x += x_step
        except ValueError:
            my_list[3].append(x), my_list[2].append(None)

    # Работаем с файлом, если его нет создаем.
    file = open('results.txt', 'w')
    file.write(f'{my_list[0]}\n{my_list[1]}\n{my_list[2]}')
    file.close()

    my_list = []  # Очищаем список.

    # Открываем файл для чтения
    file = open('results.txt', 'r')
    for line in file:
        my_list.append(line.strip().split())  # На каждой линии свое значение
    file.close()

    for i in range(len(my_list[0])):
        print(f'g(x) = {my_list[0][i]}')

    for i in range(len(my_list[1])):
        print(f'f(x) = {my_list[1][i]}')

    for i in range(len(my_list[2])):
        print(f'y(x) = {my_list[2][i]}')

    re = input('Если вы хотите выйти, нажмите 1, в ином случае наберите любое другое значение\n')
    if re == '1':
        break
    else:
        print('Программа повторяется заново...')
        continue
