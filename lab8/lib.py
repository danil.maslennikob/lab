# Для вычисления диаметра
def point_aff(radius, x1, y1, x0_and_y0):
    d = radius**2 - (x0_and_y0[0]-x1)**2 - (x0_and_y0[1]-y1)**2
    if d >= 0:
        return True
    else:
        return False

# Количество посчитанных чисел
def counter_points(radius, x, y, x0_and_y0):
    count = 0
    for x1, y1 in zip(x, y):
        if point_aff(radius, x1, y1, x0_and_y0) is True:
            count += 1
    return count
